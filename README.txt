Implementation of a K-means in python.

The K-means algorithm chooses centroids that minimize the inertia, or within-cluster sum of squares. It picks the centroids in order to maximize the coverage into a certain class, but it does not maximize the distance between the different clusters.

The implementation:
	1. Pick randomly K centroids
	2. Compute the distance between every point and every centroid
	3. Assign each point to its closest centroid
	4. Update centroids to be the mean of the cluster points
	Then loop over the step 2 to 4 until converging with a result, you can stop when you find the same clustering architecture as the previous step

It is possible to improve this algorithm with methods like Batch K-means.

In order to execute the program:

	$ cd src/
	$ python3 main.py
